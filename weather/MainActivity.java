package com.example.mohammad.weather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohammad.weather.models.Weather;
import com.example.mohammad.weather.models.WeatherModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    TextView dateView, cityname, tempview;
    EditText city;
    Button search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cityname = (TextView) findViewById(R.id.cityname);
        dateView = (TextView) findViewById(R.id.dateView);
        tempview = (TextView) findViewById(R.id.tempview);
        city = (EditText) findViewById(R.id.city);
        search = (Button) findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWeatherByAsync();

            }
        });
    }

    void getWeatherByAsync() {
        try{  String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"
                +city.getText().toString() +"%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
            AsyncHttpClient client = new AsyncHttpClient();
            client.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                    Toast.makeText(MainActivity.this, "Erros In conne cting", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    parseByGSon(responseString);

                }
            });}
        catch (Exception e) {


            Toast.makeText(this, "Connection Failed ", Toast.LENGTH_SHORT).show();

        }}

    void parseByGSon(String response) {

        Gson gsone = new Gson();
        WeatherModel weatherModel =
                gsone.fromJson(response, WeatherModel.class);

        cityname.setText(weatherModel.getQuery().getResults().getChannel().getLocation().getCity());
        dateView.setText(weatherModel.getQuery().getResults().getChannel().getItem().getCondition().getDate());
        tempview.setText(weatherModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp());

        double Ctemp = 0 ;
        int Ftemp = Integer.parseInt( weatherModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp());
        Ctemp =(Ftemp - 32 )* 0.5 ;
        tempview.setText("Temp In Celsiouse:" + Ctemp);
    }



    void parseServerRespons(String response) {
        Log.d("webservice_debug", response);


    }
}
